from django.conf import settings
from django import forms
from django_mako_plus import view_function
from django.contrib.postgres.search import SearchVector

from homepage import models as hmod
from .. import dmp_render, dmp_render_to_string

from formlib.form import FormMixIn
import csv
from django.db.models import Q

@view_function
def process_request(request):
    '''Shows the quotes, processing the search form if submitted.'''
    quoteqry = hmod.Quote.objects.all()

    if (request.method == "POST"):
        print('IN POST!!!!!', ' ', request.GET)
        searchtext = request.POST.get('userSearch', None)

    searchTextCSV = ""
    form = SearchForm(request)
    if form.is_valid():
        searchTextCSV = form.commit()
        print('>>>>>>>>SEARCH TEXT CSV: ', searchTextCSV)
        length = len(searchTextCSV)
        print('FIRST ITEM: ', searchTextCSV[0])
        combinedfilter = "Q(search='" + searchTextCSV[0]+ "')"
        for i in range(1, length):
            combinedfilter = combinedfilter + "& Q(search='" + searchTextCSV[i]+ "')"
        print('COMBINED FILTER: ', combinedfilter)
        # quoteqry = hmod.Quote.objects.annotate(search=SearchVector('text', 'author__first_name', 'author__last_name')).filter(combinedfilter)
        quoteqry = hmod.Quote.objects.annotate(search=SearchVector('text', 'author__first_name', 'author__last_name')).filter(search=searchTextCSV[0])
        print('>>>>NEW QRY: ', quoteqry)

    #Create Table to be passed to the HTML.  Iterate through all of the quoteitems in quoteqry
    table = QuotesTable()
    for i in quoteqry:
        # Quote ID	Author	Quote Text	Tags
        tags = ""
        tagsqry = hmod.QuoteTag.objects.filter(quote = i.id)
        for each in tagsqry:
            if(tags == ""):
                tags = each.tag.text
            else:
                tags = tags + '; ' + each.tag.text
        print('TAGS: ', tags)
        table.append([ i.id, i.author.first_name + ' ' + i.author.last_name, i.text, tags ])
    print(table)



    # render the context
    context = {
        'form': form,
        'quoteqry': quoteqry,
        'table': table,
    }
    return dmp_render(request, 'index.html', context)

class SearchForm(FormMixIn, forms.Form):

    def init(self):
        self.fields['userSearch'] = forms.CharField(required=False)

    def clean(self):
        userSearch = self.cleaned_data.get('userSearch')
        lines = [userSearch]
        for row in csv.reader(lines, delimiter=" "):
            print('>>>>>>>USERSEARCH: ', row)

        self.row = row
        return self.cleaned_data

    def commit(self):
        return self.row




class QuotesTable(list):
    '''
    A simple class to create an HTML table.
    This inherits from list, so add to it just as you would any other list in python:

        qry = hmod.Quote.objects.....()

        table = QuotesTable()
        table.append([ 'Cell 1a', 'Cell 1b', 'Cell 1c' ])
        table.append([ 'Cell 2a', 'Cell 2b', 'Cell 2c' ])
        print(table)
    '''
    table_id = 'quotes_table'
    headings = [
        'Quote ID',
        'Author',
        'Quote Text',
        'Tags',
    ]

    def __str__(self):
        '''
        Prints the html for the table.
        '''
        # start the table
        html = []
        html.append('<table id="{}">'.format(self.table_id))
        # table headings
        html.append('<thead>')
        html.append('<tr class="header_row">')
        for val in self.headings:
            html.append('<th>{}</th>'.format(val))
        html.append('</tr>')
        html.append('</thead>')
        # table data
        html.append('<tbody>')
        for row_i, row in enumerate(self):
            html.append('<tr class="data_row" data-row="{}">'.format(row_i))
            for val in row:
                html.append('<td>{}</td>'.format(val))
            html.append('</tr>')
        html.append('</tbody>')
        # end the table and return
        html.append('</table>')
        return '\n'.join(html)
